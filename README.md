# README #

This repository contains a data set of duplicate questions and misflagged duplicate questions from the same 12 Stackexchange (http://stackexchange.com/) subforums that are used in CQADupStack (http://nlp.cis.unimelb.edu.au/resources/cqadupstack/). The StackExchange data dump that forms the basis of this set is the version released on December 15, 2016.

Please cite the following paper when making use of this data set:

    @inproceedings{hoogeveen2018,
        author = {Hoogeveen, Doris and Bennett, Andrew and Li, Yitong and Verspoor, Karin M. and Baldwin, Timothy},
        title = {Detecting Misflagged Duplicate Questions in Community Question-Answering Archives},
        booktitle = {Proceedings of the International AAAI Conference on Web and Social Media (ICWSM)},
        year = {2018},
        publisher = {AAAI}
    }

For licensing information please see LICENCE.txt, which you'll find in MisflaggedDuplicates.zip.

The data is divided into a train, dev, and test set. Each of these contains one pair of questions per line. Each line starts with an ID of the question pair, followed by a label ('closed' or 'reopened'), followed by the text of question 1 and the text of question 2. The fields are TAB separated. The label 'closed' means that the question was correctly closed as a duplicated. 'Reopened' means it was misflagged as a duplicate, and was later reopened.
The ID of the question pair contains the name of the StackExchange subforum the questions came from, and the original StackExchange IDs of the two questions. With this information you can find the questions online. For instance, the questions from question pair android-98788-44608 can be found at the following two URLs: https://android.stackexchange.com/questions/98788/ and https://android.stackexchange.com/questions/44608/.

The json folder contains one zipfile per subforum. After unzipping, each subforum consists of four JSON files: subforum\_questions.json, subforum\_answers.json, subforum\_comments.json and subforum\_users.json.

The JSON files are structured in the same way as the CQADupStack dataset (http://nlp.cis.unimelb.edu.au/resources/cqadupstack/), except for the 'history' field in subforum\_questions.json, which is not there in CQADupStack. Here's an overview of the structure:

Questions:

     { postid (str): {
        "body": "raw text of the body of the question",
        "viewcount": int,
        "dups": [list of duplicate postids],
        "title": "raw text of the title of the question",
        "tags": [list of tags],
        "userid": userid (str),
        "related": [list of related postids],
        "score": int,
        "answers": [list of answerids],
        "acceptedanswer": accepted answerid (str),
        "creationdate": "2013-08-23T13:17:44.483",
        "favoritecount": int, (More information on what that means can be found here: http://meta.stackexchange.com/questions/53585/how-do-favorite-questions-work)
        "comments": [list of commentids]
        "history": {date of action: {"action": "closed", (This is always "closed", because it is the only action we are interested in. The original StackExchange dump contains other actions
                                                          too though, including actions like deleting a post, locking it from receiving more answers, making it community owned, migrating 
                                                          it, etc. See https://archive.org/details/stackexchange.)
                                     "voters": [list of userids of users who voted for this action],
                                     "deciding_vote": userid of the voter who cast the deciding vote (str), (This can be either a user with a gold badge, or the fifth person to vote for the action.)
                                     "orig_question_ids": [list of question ids that this vote is about. If a question is not reopened, then this is the same as the ids in "dups"],
                                     "merged": true/false, (whether two questions were merged or only linked. Linked questions exist as two separate entiries. If a question is merged with another 
                                                            one, then you are automatically redirected to the canonical one if you visit the url of the other one.)
                                     "reopened": {"reopened_date": the date on which the question was reopened,
                                                  "voters": [list of userids of voters]
                                                  "deciding_vote": userid of deciding voter (str), (This can be either a user with a gold badge, or the fifth person to vote for reopening.)
                                                 }
                                     (If "reopened" is empty, it means the question was closed as a duplicate of another question and never reopened. If reopened is filled, then the question has been reopened.)
                                    }
                    date of action: { Etc.
                    }
        },
        postid (str): {
        Etc.
        }
    }

Answers:

     { answerid (str): {
        "body": "raw text of the answer",
        "userid": userid (str),
        "comments": [list of commentids],
        "score": int,
        "parentid": id of question this is an answer to (str),
        "creationdate": "2011-07-17T13:15:06.457"
      },
      answerid (str): {
      Etc.
      }
    }

Comments:

     { commentid (str): {
        "body": "raw text of the comment",
        "userid": userid (str),
        "parenttype": "answer" or "question" depending on what this is a comment to,
        "score": int,
        "parentid": answerid (str) or postid (str) depending on what this is a comment to,
        "creationdate": "2013-08-27T13:06:47.970"
      },
      commentid (str): {
      Etc.
      }
    }

Users:

     { userid (str): {
        "views": int,
        "rep": int, (Information on what this means and how it is calculated can be found here http://stackoverflow.com/help/whats-reputation)
        "lastaccessdate": "2014-01-24T15:38:51.037",
        "answers": [list of ids of answers posted by this user],
        "questions": [list of ids of questions posted by this user],
        "upvotes": int,
        "downvotes": int,
        "badges": [list of badges earned by this user], (Information on what badges are and which ones can be earned can be found here: http://stackoverflow.com/help/badges)
        "date_joined": "2014-01-05T19:21:27.017"
      },
      userid (str): {
      Etc.
      }
    }

The train, dev, and test files contain only a subset of all the questions in the JSON files. The full set of questions is included in the JSON files, because this allows people to use the real user data in the construction of features, instead of an artificial subset of it. The total number of questions asked by a user for instance, the text from other questions asked by a user, etc. depending on your use case.

The questions in misflagged\_duplicates\_train|dev|test.txt have been preprocessed using the `very_basic_cleaning()` method of CQADupStack's data manipulation script (https://github.com/D1Doris/CQADupStack/).

For questions please contact Doris Hoogeveen at doris dot hoogeveen at gmail.

